import { ApiRequest } from "../request";

const baseUrl: string = global.appConfig.baseUrl;

export class AuthController {
    async registerController(emailValue: string, passwordValue: string, name: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Register`)
            .body({
                email: emailValue,
                password: passwordValue,
                userName: name
            })
            .send();
        return response;
    }

    async login(emailValue: string, passwordValue: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Auth/login`)
            .body({
                email: emailValue,
                password: passwordValue,
            })
            .send();
        return response;
    }
}
