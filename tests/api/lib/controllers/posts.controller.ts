import { ApiRequest } from "../request";

const baseUrl: string = global.appConfig.baseUrl;

export class PostsController {
    
    async newPost(userID: string, previewImageValue: string, bodyValue:string, token1:string){
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url('api/Posts')
            .bearerToken(token1)
            .body({
                authorId: userID,
                previewImage: previewImageValue,
                body: bodyValue
            })
            .send();
        return response;
    }
     
    async getAllPosts() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url('api/Posts')
            .send();
        return response;
    }

    async createNewPost(userID: string, previewImageValue: string, bodyValue: string,token1:string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url('api/Posts')
            .bearerToken(token1)
            .body({
                    authorId: userID,
                    previewImage: previewImageValue,
                    body: bodyValue
                 })
            .send();
        return response;
    }
  
    async getPostById(id) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`api/Posts/${id}`)
            .send();
        return response;
    }

    async createNewCommentToPost(userID: string, postID: string, bodyValue: string, token1:string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Comments`)
            .bearerToken(token1)
            .body({
                authorId: userID,
                postId: postID,
                body: bodyValue
            })
            .send();
        return response;
    }

    async createNewLikeToPost(postID: string, Like: boolean, userID, token1) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts/Like`)
            .bearerToken(token1)
            .body({
                entityId: postID,
                isLike: Like,
                userId: userID
            })
            .send();
        return response;
    }
    

   
}
