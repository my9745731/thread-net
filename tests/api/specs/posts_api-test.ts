import { expect } from "chai";
import { PostsController } from "../lib/controllers/posts.controller"; 
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const a = new AuthController();
const u = new UsersController();
const p = new PostsController();
let token1: string;
let userID: string;
let postID: string;
let like: boolean = true;

describe(`Posts controller`, () => {
    
it('Login', async()=>{
    let response=await a.login("Ivan.i@gmail.com", "123QWE");
    userID = response.body.user.id;
    token1 = response.body.token.accessToken.token;
    console.log('The userID is: ' + userID);
    console.log('The user token is: ' + token1);
    checkStatusCode(response, 200);
    checkResponseTime(response,1000);
});

it('Login with invalid credentials email: "Ivan.i@gmail.com", "123QWE"', async()=>{
    let response= await a.login("Ivan.i@gmail.com", "123")
    checkStatusCode(response, 401);
    checkResponseTime(response,1000);
});

it('GetAllPosts', async () => {
    let response_allPosts = await p.getAllPosts();
    checkStatusCode(response_allPosts, 200);
    checkResponseTime(response_allPosts,3000);
});

it('createNewPost', async () => {
    let response_create=await p.createNewPost(userID, "string", "This is My New Post", token1);
    postID = response_create.body.id;
    console.log('The post id is = '+postID);
    checkStatusCode(response_create, 200);
    checkResponseTime(response_create,1000);
});

it('createNewComment', async () => {
    let response_create=await p.createNewCommentToPost(userID, postID, "Thanks, this post is great!", token1);
    console.log('The new comment to post = '+postID);
    checkStatusCode(response_create, 200);
    checkResponseTime(response_create,1000);  
});

it('createNewLike', async () => {
    let response_create=await p.createNewLikeToPost(postID, true, userID, token1);
    console.log('The new like to post = '+response_create.body);
    checkStatusCode(response_create, 200);  
    checkResponseTime(response_create,1000);
});

})