import { expect } from "chai";
import { AuthController } from "../lib/controllers/auth.controller";
import { UsersController } from "../lib/controllers/users.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";


const a = new AuthController();
const u = new UsersController();
let token1: string;
let userID: string;
let accessToken: string;
let userDataBeforeUpdate, userDataToUpdate;

describe(`Users controller`, () => {

it('Register', async()=>{
  let response_register=await a.registerController("Ivan.i@gmail.com", "123QWE", "Ivan");
  console.log(response_register.body);
  checkStatusCode(response_register, 201);
  checkResponseTime(response_register,1000);
});

it('GetAllUsers', async () => {
  let response= await u.getAllUsers();
  checkStatusCode(response, 200);
  checkResponseTime(response,2000);
});

it('Login', async()=>{
  let response=await a.login("Ivan.i@gmail.com", "123QWE");
  userID = response.body.user.id;
  token1 = response.body.token.accessToken.token;
  console.log('The userID is: ' + userID);
  console.log('The user token is: ' + token1);
  checkStatusCode(response, 200);
  checkResponseTime(response,1000);
});

it('getCurrentUser', async () => {
  let response_from_token = await u.getCurrentUser(token1);
  console.log('hgvhkgvhgvhgvhg: '+response_from_token.body.userName);
  userDataBeforeUpdate = response_from_token.body;
  checkStatusCode(response_from_token, 200);
  checkResponseTime(response_from_token,1000); 
});

it('get update username using valid data', async () => {
  // replace the last 3 characters of actual username with random characters.
  // Another data should be without changes
  function replaceLastThreeWithRandom(str: string): string {
      return str.slice(0, -3) + Math.random().toString(36).substring(2, 5);
  }
  userDataToUpdate = {
    id: userDataBeforeUpdate.id,
    avatar: userDataBeforeUpdate.avatar,
    email: userDataBeforeUpdate.email,
    userName: replaceLastThreeWithRandom(userDataBeforeUpdate.userName),
  };
  let response_user = await u.updateUser(userDataToUpdate, token1);
  console.log(userDataToUpdate.userName);
  checkStatusCode(response_user, 204);
  checkResponseTime(response_user,1000); 
});

it('getCurrentUser', async () => {
  let response_from_token = await u.getCurrentUser(token1);
  console.log(response_from_token.body);
  userDataBeforeUpdate = response_from_token.body;
  checkStatusCode(response_from_token, 200);
  checkResponseTime(response_from_token,1000); 
});

it('getUserById', async () => {
  let response_ByID = await u.getUserById(userID);
  console.log('information about user: '+ response_ByID.body);
  checkStatusCode(response_ByID, 200);
  checkResponseTime(response_ByID,1000); 
});

it('deleteUserById', async () => {
  let response_Del = await u.deleteUserById(userID,token1);
  console.log(response_Del.body);
  //checkStatusCode(response_Del, 204);
  checkResponseTime(response_Del,1000); 
});

});




   

    
    
    
    



    

